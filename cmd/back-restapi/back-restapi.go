package main

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	// db := database.InitDB()
	// defer db.Close()
	router := mux.NewRouter()
	router.HandleFunc("/user", CreateUser).Methods("POST")
	router.HandleFunc("/user", GetUser).Methods("GET")
	router.HandleFunc("/user", UpdateUser).Methods("PUT")
	router.HandleFunc("/user", DeleteUser).Methods("DELETE")

	router.HandleFunc("/pass", CreatePass).Methods("POST")
	router.HandleFunc("/pass", GetPass).Methods("GET")
	router.HandleFunc("/pass", UpdatePass).Methods("PUT")
	router.HandleFunc("/pass", DeletePass).Methods("DELETE")

	router.HandleFunc("/place", CreatePlace).Methods("POST")
	router.HandleFunc("/place", GetPlace).Methods("GET")
	router.HandleFunc("/place", UpdatePlace).Methods("PUT")
	router.HandleFunc("/place", DeletePlace).Methods("DELETE")

	httpServer := &http.Server{Addr: ":8000", Handler: router}
	httpServer.ListenAndServe()
}

type User struct {
	Firstname   string `json:"firstname"`
	Lastname    string `json:"lastname"`
	Age         uint8  `json:"age"`
	PhoneNumber string `json:"phonenumber"`
	Address     string `json:"address"`
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// result, err := db.Exec("INSERT INTO users VALUES(?, ?, ?, ?, ?)", user.Firstname, user.Lastname, user.Age, user.PhoneNumber, user.Address)
    // if err != nil {
    //     log.Fatal(err)
    // }

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
    _ = json.NewEncoder(w).Encode(&user)
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	// Get user
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	// Update user
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	// Delete user
}

func CreatePass(w http.ResponseWriter, r *http.Request) {
	// Create pass
}

func GetPass(w http.ResponseWriter, r *http.Request) {
	// Get pass
}

func UpdatePass(w http.ResponseWriter, r *http.Request) {
	// Update pass
}

func DeletePass(w http.ResponseWriter, r *http.Request) {
	// Delete pass
}

func CreatePlace(w http.ResponseWriter, r *http.Request) {
	// Create place
}

func GetPlace(w http.ResponseWriter, r *http.Request) {
	// Get place
}

func UpdatePlace(w http.ResponseWriter, r *http.Request) {
	// Update place
}

func DeletePlace(w http.ResponseWriter, r *http.Request) {
	// Delete place
}

// get (check if user can go)
// get (all places user can go)
